import React from 'react';
import logo from './logo.svg';
import './App.css';
//import Pembelian from './Tugas-9/Pembelian.js'
//import HargaBuah from './Tugas-10/HargaBuah.js'
//import Clock from './Tugas-11/Clock.jsx';
//import Form from './Tugas-12/Form.jsx';
//import Tugas14 from './Tugas-14/Tugas14.jsx';
import { BrowserRouter as Router } from "react-router-dom";
import Nav from './Tugas-15/Nav'
import Routes from './Tugas-15/Routes'



function App() {
  return (
    <div className="App">
        <Router>
            <Nav />
            <Routes />
        </Router>
    </div>
   
  );
}

export default App;
