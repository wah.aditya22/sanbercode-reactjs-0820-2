import React, { useState, useContext, useEffect } from 'react';
import Axios from 'axios';

import { FruitContext } from './FruitContext';

const FruitForm = () => {
  const [, setFruitList, selectedFruit, setSelectedFruit] = useContext(FruitContext);
  const [fruitName, setFruitName] = useState('');
  const [fruitPrice, setFruitPrice] = useState(0);
  const [fruitWeight, setFruitWeight] = useState(0);

  useEffect(() => {
    setFruitName(selectedFruit.name);
    setFruitPrice(selectedFruit.price ?? 0);
    setFruitWeight(selectedFruit.weight ?? 0);
  }, [selectedFruit])

  const requestData = () => {
    Axios.get('http://backendexample.sanbercloud.com/api/fruits')
      .then((res) => setFruitList(res.data))
      .catch((err) => console.log(err))
  }

  const onPriceChangeHandler = (e) => {
    const price = isNaN(e.target.value) ? 0 : +e.target.value;
    setFruitPrice(price);
  }

  const onWeightChangeHandler = (e) => {
    const weight = isNaN(e.target.value) ? 0 : +e.target.value;
    setFruitWeight(weight);
  }

  const onInsertHandler = () => {
    const fruit = { name: fruitName, price: fruitPrice, weight: fruitWeight };
    Axios.post('http://backendexample.sanbercloud.com/api/fruits', fruit)
      .then((_) => {
        requestData();
        onFormReset();
      })
      .catch((err) => console.log(err))
  }

  const onSaveHandler = () => {
    const fruit = { name: fruitName, price: fruitPrice, weight: fruitWeight };

    Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedFruit.id}`, fruit)
      .then((_) => {
        requestData()
        onFormReset();
      })
      .catch((err) => console.log(err))
  }

  const onFormReset = () => {
    setFruitName('');
    setFruitPrice(0);
    setFruitWeight(0);
    setSelectedFruit({});
  }

  return (
    <div className="content">
      <h1 className="centered">Form Buah</h1>
      <div className="form-group">
        <div className="form-label"><label htmlFor="txtName">Nama Buah</label></div>
        <input
          placeholder="Nama Buah"
          value={fruitName ?? ''}
          onChange={(e) => setFruitName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <div className="form-label"><label htmlFor="txtName">Harga Buah</label></div>
        <input
          placeholder="Harga Buah"
          value={+fruitPrice}
          onChange={onPriceChangeHandler}
        />
      </div>
      <div className="form-group">
        <div className="form-label"><label htmlFor="txtName">Harga Buah</label></div>
        <div style={{ position: 'relative', display: 'inline' }}>
          <input
            placeholder="Berat Buah (gr)"
            value={+fruitWeight}
            onChange={onWeightChangeHandler}
          />
          <span style={{ position: 'absolute', right: '5px' }}><b>gr</b></span>
        </div>
      </div>
      {
        selectedFruit.id ?
          <button onClick={onSaveHandler}>Save</button> :
          <button onClick={onInsertHandler}>Insert</button>
      }
    </div>
  )
}

export default FruitForm;
