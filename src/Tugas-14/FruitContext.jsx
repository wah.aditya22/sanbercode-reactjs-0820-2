import React, {useState, createContext} from 'react'

export const FruitContext = createContext();

export const FruitProvider = (props)=>{
    const [fruitList, setFruitList] = useState([])
    const [selectedFruit, setSelectedFruit] = useState({});

    return(
        <FruitContext.Provider value={[fruitList, setFruitList, selectedFruit, setSelectedFruit]}>
            {props.children}
        </FruitContext.Provider>
    )
}