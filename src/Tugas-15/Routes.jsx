import React from "react";
import Pembelian from '../Tugas-9/Pembelian'
import HargaBuah from '../Tugas-10/HargaBuah'
import Clock from '../Tugas-11/Clock'
import Form from '../Tugas-12/Form'
import Hooks from '../Tugas-13/Hooks'
import Tugas14 from '../Tugas-14/Tugas14'
import { Switch, Route } from "react-router";

const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
        <Tugas14 />
      </Route>
      <Route path="/tugas13">
        <Hooks />
      </Route>
      <Route exact path="/tugas12">
        <Form />
      </Route>
      <Route exact path="/tugas11">
        <Clock />
      </Route>
      <Route path="/tugas10">
        <HargaBuah />
      </Route>
      <Route exact path="/tugas9">
        <Pembelian />
      </Route>
    </Switch>
  );
};

export default Routes;
