import React, {useState, useEffect, createContext} from 'react'

export const NavColorContext = createContext()

export const NavColorProvider = props => {
  const [navColor, setNavColor] = useState('default')

  return (
    <NavColorContext.Provider value={[navColor, setNavColor]}>
      {props.children}
    </NavColorContext.Provider>
  )
}
